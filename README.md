# Payload validation with JSON Schema

Your task is to implement an API endpoint that checks a payload for validity given the name and version of a validation schema.

Example:

Calling the API via POST `http://localhost:8080/validate/simple/2`

- should accept (200) a payload `{ "number": 15 }`
- but decline (400) a payload `{ "number": "abc" }`.

More examples can be found in the tests.

## Before you get started

### Get the development environment up and running

Requirements:

- `docker`, `docker-compose` installed locally
- `npm` or `yarn` installed locally

Install dependencies:

```bash
yarn
```

Start the docker-compose environment:

```bash
docker-compose up
```

The development environment also includes a test watcher. It is intended that some tests fail at this point ;)

### Get familiar with the code

Look through the files and get an understanding of how things work together and which parts still need to be implemented.

## What we expect from you

1. Make the provided API tests pass.
2. In case you feel the urge to change/clean up the code, apply best practices, demo your skills, add a database here and there,etc. ... feel free to do so. ;)
3. When you are ready, submit your solution (zip file or a link to a repository via email).

## Need help?

In case of problems with the given project, please let us know (e.g. via email).

## Time estimate

The challenge should be solvable within a few hours.

We expect your solution around 1-2 weeks after you got the challenge.

We will take a look and invite you to a review/feedback round with the team.

There you get the chance to walk us through your solution, point out things you are specifically happy with or also things you would like to discuss further. We will also ask our questions and by talking about your solution you will also have the chance to get to know the team you might be working with in the future :)
