import request from 'supertest'

import server from '../server'

describe('/health', () => {
  it('returns okay and 200 if everything is okay', () =>
    request(server())
      .get('/health')
      .expect(200)
      .expect(({ body }) => expect(body).toEqual({ status: 'ok' })))
})
