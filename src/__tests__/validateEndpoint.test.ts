import request from 'supertest'

import server from '../server'

describe('/validate/:schemaName/:version', () => {
  const app = request(server())

  it.each([
    ['simple', 300],
    ['does not exist', 1],
    ['$øø-Bæ¶', 1337],
  ])(
    'returns a 404 if the schemaName or version is invalid or the schema does not exist (/validate/%s/%s)',
    (schemaName, version) =>
      app.post(`/validate/${schemaName}/${version}`).expect(404)
  )

  it.each([
    ['simple', 1, { number: -1, notANumber: 'abc' }],
    ['simple', 2, { number: 15 }],
    ['some-animal', 1, { name: 'cat', legs: 4, sound: 'meow' }],
  ])(
    'accepts a valid set of data for a given schema (/validate/%s/%s, %s)',
    (schemaName, version, validPayload) =>
      app
        .post(`/validate/${schemaName}/${version}`)
        .send(validPayload)
        .expect(200)
        .expect(({ body }) => expect(body).toEqual({ status: 'ok' }))
  )

  it.each([
    ['simple', 1, { number: 'NAN' }],
    ['simple', 2, { number: 1 }],
    ['some-animal', 1, { name: 'elephant', canSwim: true, legs: 4 }],
  ])(
    'does not accept an invalid set of data for a given schema  (/validate/%s/%s, %s)',
    (schemaName, version, invalidPayload) =>
      app
        .post(`/validate/${schemaName}/${version}`)
        .send(invalidPayload)
        .expect(400)
        .expect(({ body }) =>
          expect(body).toEqual({ status: 'payload invalid' })
        )
  )
})
