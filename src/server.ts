import express from 'express'
import http from 'http'
import { createTerminus } from '@godaddy/terminus'

import errorRequestHandler from './requestHandlers/errorHandler'
import validateHandler from './requestHandlers/validateHandler'

const server = () => {
  const app = express()

  app.use(express.json())

  app.post('/validate/:schemaName/:version', validateHandler)

  app.use(errorRequestHandler)

  const httpServer = http.createServer(app)

  createTerminus(httpServer, {
    healthChecks: {
      '/health': () => Promise.resolve(),
    },
  })

  return httpServer
}

export default server
