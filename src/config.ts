import convict from 'convict'

interface AppConfiguration {
  port: number
}

const appConfiguration = convict<AppConfiguration>({
  port: {
    format: 'port',
    default: 8080,
    env: 'PORT',
  },
})

export default appConfiguration
