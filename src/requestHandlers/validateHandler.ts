import type { RequestHandler } from 'express'

/**
 * Request handler for validating payloads
 * 
 * (Acceptable schemas can be found here: <rootDir>/schemas/)
 * 
 * @param req 
 * @param res 
 * @param next 
 * @returns 
 */
const validateRequestHandler: RequestHandler = (req, res, next) => {
  try {
    // TODO
    return res.status(200).json({ status: 'ok' })
  } catch (error) {
    return next(error)
  }
}

export default validateRequestHandler
