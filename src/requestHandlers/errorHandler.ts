import type { ErrorRequestHandler } from 'express'
import NotFoundError from '../utils/Errors/NotFoundError'

const errorRequestHandler: ErrorRequestHandler = (
  error,
  request,
  response,
  next
) => {
  if (error instanceof NotFoundError) {
    return response
      .status(404)
      .json({ error: 'requested resource could not be found' })
  }
}

export default errorRequestHandler
