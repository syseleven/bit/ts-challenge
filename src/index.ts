import server from './server'
import config from './config'

const port = config.get('port')

server().listen(port, () => console.log('listening'))
