import type { InitialOptionsTsJest } from 'ts-jest'

const jestConfig: InitialOptionsTsJest = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  rootDir: 'src',
}

export default jestConfig
